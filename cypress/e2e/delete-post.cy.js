describe('template spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:8080')
    cy.get('[href="/posts"]').click()
    cy.get(':nth-child(1) > post-ui > #title').click()
    cy.get('.botones-post-details > :nth-child(3)').click()
    //Scroll to the bottom of the page
    cy.wait(1000)
    cy.get('.posts-nav').scrollTo('bottom')
    cy.get(':last-child > post-ui > #title').click()
    cy.get('.botones-post-details > :nth-child(3)').click()
  })
})