describe('template spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:8080')
    cy.get('[href="/posts"]').click()
    cy.get('#title-input').type('New post')
    cy.get('#content-input').type('New post content')
    cy.get('[type="submit"]').click()
    cy.wait(1000)
    cy.get(':nth-child(1) > post-ui > #title').click()
    cy.get('post-details > h2').should('have.text', 'New post')
    cy.get('post-details > p').should('have.text', 'New post content')
  })
})