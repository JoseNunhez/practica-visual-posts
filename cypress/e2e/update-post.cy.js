describe("template spec", () => {
  it("passes", () => {
    cy.visit("http://localhost:8080");
    cy.get('[href="/posts"]').click();
    cy.get(":nth-child(1) > post-ui > #title").click();
    cy.get(".botones-post-details > :nth-child(2)").click();
    cy.get("#update-title-input").clear();
    cy.get("#update-title-input").type("Update post");
    cy.get("#update-content-input").clear();
    cy.get("#update-content-input").type("Update post content");
    cy.get('[type="submit"]').click();
    cy.get("ul li").first().click();
  });
});
