import { DeletePostRepository } from "../src/repositories/delete-post.repository";
import { DeletePostUseCase } from "../src/usecases/delete-post.usecase";
import { POSTS } from "../src/repositories/posts";

jest.mock("../src/repositories/delete-post.repository");
describe("Delete an post use case", () => {
  beforeEach(() => {
    DeletePostRepository.mockClear();
  });

  it("Should delete an entry", async () => {
    DeletePostRepository.mockImplementation(() => {
      return {
        deletePost: () => {},
      };
    });

    const post = {
        id: 1,
        title: "title",
        content: "content",
        };

    const response = await DeletePostUseCase.execute(post, POSTS);


    expect(response.length).toBe(POSTS.length - 1);
    expect(response[0].id).toBe(2);
  });
});
