import { Post } from "../src/model/post";
import { UpdatePostRepository } from "../src/repositories/update-post.repository";
import { UpdatePostUseCase } from "../src/usecases/update-post.usecase";
import { POSTS } from "../src/repositories/posts";

jest.mock ("../src/repositories/update-post.repository");

describe("Edit post use case tests", () => {
  const post = new Post({
    id: 1,
    title: "title",
    content: "content",
  });

  beforeEach(() => {
    UpdatePostRepository.mockClear();
  });

  it("should update a post", async () => {
    const POSTS = [
      {
        id: 1,
        title: "title",
        content: "content",
      },
     {
        id: 2,
        title: "title 2",
        content: "content 2",
      },
    ];
    UpdatePostRepository.mockImplementation(() => {
      return {
        updatePost: () => {
          return {
            id: post.id,
            title: post.title,
            content: post.content,
          };
          }
        };
      });
  
    const response = await UpdatePostUseCase.execute(post, POSTS);

    expect(response.length).toBe(2);
    expect(response[0].id).toBe(1);
    expect(response[0].title).toBe("title");
    expect(response[1].id).toBe(2);
    expect(response[1].title).toBe("title 2");
  });
});
