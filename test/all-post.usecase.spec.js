import { POSTS } from "./fixtures/posts"
import { PostsRepository } from "../src/repositories/posts.repository";
import { AllPostsUseCase } from "../src/usecases/all-posts.usecase";

jest.mock("../src/repositories/posts.repository");

describe("All posts use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should get all POSTS", async () => {
    PostsRepository.mockImplementation(() => {
      return {
        getAllPosts: () => {
          return POSTS;
        },
      };
    });

    const allPosts= await AllPostsUseCase.execute();

    expect(allPosts.length).toBe(100);

    expect(allPosts[0].title).toBe(POSTS[0].title);
    expect(allPosts[0].content).toBe(POSTS[0].body);
  });
});