import { Post } from "../src/model/post.js";
import { AddPostUseCase } from "../src/usecases/add-post.usecase.js";
import { AddPostRepository } from "../src/repositories/add-post.repository.js";
import { POSTS } from "./fixtures/posts.js";


jest.mock ("../src/repositories/add-post.repository");
describe("Add post use case tests", () => {
    const post = new Post({
        id: 1,
        title: "title updated",
        content: "content updated",
    });

    beforeEach(() => {
        AddPostRepository.mockClear();
    });

    it("should add a post", async () => {
        AddPostRepository.mockImplementation(() => {
            return {
                addPost: () => {
                    return {
                        id: post.id,
                        title: post.title,
                        content: post.content,
                    };
                },
            };
        });

        const response = await AddPostUseCase.execute(post, POSTS);

        expect(response.length).toBe(101);
        expect(response[0].id).toBe(1);
        expect(response[0].title).toBe("title updated");
    });
});