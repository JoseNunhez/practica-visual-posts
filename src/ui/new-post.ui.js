import { LitElement, html } from "lit";

export class NewPostComponent extends LitElement {
  static get properties() {
    return {
      title: { type: String },
      content: { type: String },
    };
  }

  constructor() {
    super();
    this.title = '';
    this.content = '';
  }

  render() {
    return html`
      <h2>Create a new post</h2>
      <form>
        <label>
          Title:
          <input type="text" value="${this.title}" @input="${this.handleTitleInput}">
        </label>
        <br>
        <label>
          Content:
          <textarea @input="${this.handleContentInput}">${this.content}</textarea>
        </label>
        <br>
        <button type="submit" @click="${this.handleFormSubmit}">Create Post</button>
        <button type="button" @click="${this.handleCancelClick}">Cancel</button>
      </form>
    `;
  }

  handleTitleInput(event) {
    this.title = event.target.value;
  }

  handleContentInput(event) {
    this.content = event.target.value;
  }

  handleFormSubmit(event) {
    event.preventDefault();

    // Aquí podrías enviar los datos del nuevo post al servidor para que sean guardados en la base de datos.
    // Por ejemplo:
    fetch('/api/posts', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        title: this.title,
        content: this.content
      })
    })
      .then(response => {
        if (response.ok) {
          // El nuevo post se ha guardado correctamente, puedes cerrar el formulario de creación de posts.
          this.dispatchEvent(new CustomEvent('post-created'));
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleCancelClick() {
    // Cuando se hace clic en el botón "Cancel", puedes emitir un evento personalizado para que el componente "PostsComponent" cierre el formulario de creación de posts.
    this.dispatchEvent(new CustomEvent('cancel'));
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("new-post", NewPostComponent);
