import { LitElement, html } from "lit";

export class PostUI extends LitElement {
  static get properties() {
    return {
      post: { type: Object },
    };
  }

  render() {
    return html`<h2 id="title">${this.post?.title}</h2>`;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("post-ui", PostUI);
