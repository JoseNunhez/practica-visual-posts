import {POSTS, addPost } from "./../repositories/posts.js";
import { AddPostRepository } from "../repositories/add-post.repository.js";
import { Post } from "../model/post.js";

// export class AddPostUseCase {
//   static async execute(post) {
//     addPost(post)
//     return POSTS
//   }
// }

export class AddPostUseCase {
  static async execute(post, posts) {
    const repository = new AddPostRepository();
    const newPostApi = await repository.addPost(post);
    const calcularId = () => {
      if (newPostApi.id <= 100) {
        return newPostApi.id;
      } else {
        return posts.length + 1;
      }
    };
    const newPost = new Post({
      id: calcularId(),
      title: newPostApi.title,
      content: newPostApi.body,
    });
    console.log([newPost, ...posts])
    return [newPost, ...posts];
  }
}

