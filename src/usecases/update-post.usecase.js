import { POSTS, updatePost } from "./../repositories/posts.js";
import { UpdatePostRepository } from "../repositories/update-post.repository.js";
import { Post } from "../model/post.js";

export class UpdatePostUseCase {
  static async execute(updatePost, posts) {
    if (updatePost.id <= 100) {
    const repository = new UpdatePostRepository();
    const newPostApi = await repository.updatePost(updatePost);
    const newPost = new Post({
      id: newPostApi.id,
      title: newPostApi.title,
      content: newPostApi.body,
    });
    return posts.map((post) => {
      if (post.id === newPost.id) {
        return newPost;
      }
      return post;
    });
  }
 else {
  return posts.map((post) => {
    if (post.id === updatePost.id) {
      return updatePost;
    }
    return post;
  });
}
} 
}
