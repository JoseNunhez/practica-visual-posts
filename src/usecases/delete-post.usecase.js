import { PostsRepository } from "../repositories/posts.repository";
import { POSTS, deletePost } from "../repositories/posts.js";
import { DeletePostRepository } from "../repositories/delete-post.repository.js";

// export class DeletePostUseCase {
//   static async execute(postId) {
//     deletePost(postId);
//     return POSTS;
//   }
// }

export class DeletePostUseCase {
  static async execute(post, posts) {
    const repository = new DeletePostRepository();
    await repository.deletePost(post.id);
    return posts.filter((a) => a.id !== post.id);
  }
}
