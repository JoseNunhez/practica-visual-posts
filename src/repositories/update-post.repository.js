import axios from "axios";

export class UpdatePostRepository {
  async updatePost({ id, title, content: body }) {
    return await (
      await axios.put(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        id,
        title,
        body,
      })
    ).data;
  }
}
