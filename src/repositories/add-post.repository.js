import axios from "axios";
import { Post } from "../model/post";

export class AddPostRepository {
  async addPost(post) {
    const newpost = {
      title: post.title,
      body: post.content,
    };
    return await (
      await axios.post("https://jsonplaceholder.typicode.com/posts", newpost)
    ).data;
  }
}