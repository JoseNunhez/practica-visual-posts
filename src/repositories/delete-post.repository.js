import axios from "axios";

export class DeletePostRepository {
  async deletePost(id) {
    return await axios.delete(
      `https://jsonplaceholder.typicode.com/posts/${id}`
    );
  }
}
