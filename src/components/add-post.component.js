import { LitElement, html } from "lit";
import { AddPostUseCase } from "../usecases/add-post.usecase.js";

export class AddPostComponent extends LitElement {
  static get properties() {
    return {
      posts: { type: Array },
      title: { type: String },
      content: { type: String },
    };
  }

  constructor() {
    super();
    this.title = "";
    this.content = "";
  }

  render() {
    return html`
      <h2>Create a new post</h2>
      <form id="add-post-form">
        <label>
          Title:
          <input
            id="title-input"
            type="text"
            value="${this.title}"
            @input="${this.handleTitleInput}"
          />
        </label>
        <br />
        <label>
          Content:
          <textarea
          id="content-input" 
          @input="${this.handleContentInput}">
${this.content}</textarea
          >
        </label>
        <br />
        <section class="botones-post-details">
          <button type="submit" @click="${this.handleFormSubmit}">
            Create Post
          </button>
          <button type="button" @click="${this.handleCancelClick}">
            Cancel
          </button>
        </section>
      </form>
    `;
  }

  handleTitleInput(event) {
    this.title = event.target.value;
  }

  handleContentInput(event) {
    this.content = event.target.value;
  }

  async handleFormSubmit(event) {
    event.preventDefault();
    const newPost = {
      title: this.title,
      content: this.content,
    };
    const updatedPosts = await AddPostUseCase.execute(newPost, this.posts);
    this.updatePosts(updatedPosts);
    this.dispatchEvent(new CustomEvent("post-created"));
  }

  handleCancelClick() {
    this.dispatchEvent(new CustomEvent("cancel"));
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("new-post", AddPostComponent);

