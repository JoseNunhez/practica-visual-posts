import { LitElement, html } from "lit";
import { DeletePostUseCase } from "../usecases/delete-post.usecase";
import { UpdatePostUseCase } from "../usecases/update-post.usecase";

export class PostDetailsComponent extends LitElement {
  static get properties() {
    return {
      post: { type: Object },
      posts: { type: Array },
      isEditing: { type: Boolean },
    };
  }

  constructor() {
    super();
    this.post = {};
    this.posts = [];
    this.isEditing = false;
  }

  render() {
    return html`
      <h2>${this.post.title}</h2>
      ${this.isEditing ? this.renderEditForm() : html`<p>${this.post.content}</p>`}
      <section class="botones-post-details">
        <button @click="${() => this.handleCancelClick()}">Cancel</button>
        <button @click="${() => this.handleEditClick()}">Update</button>
        <button @click="${() => this.handleDelete()}">Delete</button>
      </section>
    `;
  }
  
  renderEditForm() {
    return html`
      <form @submit="${this.handleUpdateForm}">
        <label>
          Title:
          <input id="update-title-input" type="text" name="title" .value="${this.post.title}">
        </label>
        <label>
          Content:
          <textarea id="update-content-input" name="content">${this.post.content}</textarea>
        </label>
        <button type="submit">Save</button>
      </form>
    `;
  }

  handleCancelClick() {
    this.dispatchEvent(new CustomEvent("cancelForm"));
    this.remove();
  }

  handleEditClick() {
    this.isEditing = true;
    this.requestUpdate();
  }
  

  handleUpdateForm(event) {
    event.preventDefault();
    const formData = new FormData(event.target);
    const updatedPost = {
      ...this.post,
      title: formData.get('title'),
      content: formData.get('content'),
    };
    UpdatePostUseCase.execute(updatedPost, this.posts).then(() => {
      this.dispatchEvent(new CustomEvent("post-updated", { detail: updatedPost }));
      this.remove();
    });
  }
  
  async handleDelete() {
    console.log('eliminando post')
    const updatedPosts = await DeletePostUseCase.execute(this.post, this.posts);
    this.updatePosts(updatedPosts);
    console.log(updatedPosts)
    this.dispatchEvent(new CustomEvent("post-deleted", { detail: this.post.id }));
    this.remove();
    };

  createRenderRoot() {
    return this;
  }
}

customElements.define("post-details", PostDetailsComponent);

