import { LitElement, html } from "lit";
import { AllPostsUseCase } from "../usecases/all-posts.usecase";
import "./../ui/post.ui";
import "./add-post.component";
import "./posts-details.component";

export class PostsComponent extends LitElement {
  static get properties() {
    return {
      posts: { type: Array },
      newPostFormShown: { type: Boolean },
      selectedPost: { type: Object },
    };
  }

  constructor() {
    super();
    this.newPostFormShown = true;
    this.selectedPost = null;
    this.addEventListener("post-deleted", (e) => this.handlePostDeleted(e.detail));
    this.addEventListener("post-details-closed", () => this.handleCancelForm());
    this.addEventListener("post-created", () => this.handlePostCreated());

  }

  async connectedCallback() {
    super.connectedCallback();
    this.posts = await AllPostsUseCase.execute();
  }

  render() {
    return html`
      <article class="posts-nav">
        <button @click="${() => this.handleAddNewPostClick()}">ADD</button>
        <h1>Posts List</h1>
        <ul>
            ${this.posts?.map(
            (post) =>
              html`<li>
                <post-ui
                  .post="${post}"
                  @click="${() => this.handlePostClick(post)}"
                ></post-ui>
              </li>`
          )}
        </ul>
      </article>
      <article id="single-post" class="single-post">
        ${this.newPostFormShown
          ? html`<new-post .posts="${this.posts}"
                @post-created="${() => this.handlePostCreated()}"
                @cancel="${() => this.handleCancel()}"
                .updatePosts="${(updatedPosts) => this.posts = updatedPosts}"
              </new-post>`
          : ""}
        ${this.selectedPost
          ? html`<post-details
                .post="${this.selectedPost}"
                .posts="${this.posts}"
                @cancelForm="${() => this.handleCancelForm()}"
                @post-deleted="${(e) => this.handlePostDeleted()}"
                @post-updated="${(e) => this.handlePostUpdated(e.detail)}"
                .updatePosts="${(updatedPosts) => this.posts = updatedPosts}"
              ></post-details>`
          : ""}
      </article>
    `;
  }
  handlePostUpdated(post) {
    console.log(post)
    this.selectedPost = null;
    this.newPostFormShown = true;
    this.posts = this.posts.map((p) => p.id === post.id ? post : p);
    console.log('post actualizado')
  }

  handlePostDeleted() {
    this.selectedPost = null;
    console.log(this.posts)
    this.newPostFormShown = true;
    console.log('post eliminado')
  }
  
  handlePostClick(post) {
    this.selectedPost = post;
    this.newPostFormShown = false;
  }

  handleAddNewPostClick() {
    this.selectedPost = null;
    this.newPostFormShown = true;
  }

  handlePostCreated() {
    this.newPostFormShown = true;
  }
  

  handleCancel() {
    this.selectedPost = null;
    this.newPostFormShown = false;
  }

  handleCancelForm() {
    console.log('cancelando form')
    this.selectedPost = null;
    this.newPostFormShown = true;
  }

  createRenderRoot() {
    return this;
  }
}


customElements.define("genk-posts", PostsComponent);
